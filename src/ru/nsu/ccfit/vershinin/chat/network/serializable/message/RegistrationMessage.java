package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;

public class RegistrationMessage implements Message {
    private final Client.ClientType clientType;
    private final String name;

    public RegistrationMessage(String name, Client.ClientType clientType) {
        this.name = name;
        this.clientType = clientType;
    }

    public String getName() {
        return name;
    }

    public Client.ClientType getClientType() {
        return clientType;
    }
}
