package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;

import java.io.Serializable;
import java.util.ArrayList;

public class ServerSuccessListMessage implements Message {
    static public class NameAndType implements Serializable {
        private final String name;
        private final Client.ClientType clientType;

        public NameAndType(String name, Client.ClientType clientType) {
            this.name = name;
            this.clientType = clientType;
        }

        public String getName() {
            return name;
        }

        public Client.ClientType getClientType() {
            return clientType;
        }

        @Override
        public String toString() {
            if (clientType.equals(Client.ClientType.SERIALIZATION_TYPE)) {
                return "nickname - " + name + ", type - Serialization; ";
            } else {
                return "nickname - " + name + ", type - XML; ";
            }
        }
    }
    private final ArrayList<NameAndType> listNames;

    public ServerSuccessListMessage(ArrayList<NameAndType> listNames) {
        this.listNames = listNames;
    }

    public ArrayList<NameAndType> getListNames() {
        return listNames;
    }
}
