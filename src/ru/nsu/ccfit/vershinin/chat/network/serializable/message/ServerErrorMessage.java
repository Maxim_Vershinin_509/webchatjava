package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ServerErrorMessage implements Message {
    private final String reason;

    public ServerErrorMessage(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
