package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ClientToServerMessage implements Message {
    private final String message;
    private final int id;

    public ClientToServerMessage(String message, int id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }
}
