package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class NewClientMessage implements Message {
    private final String nickname;

    public NewClientMessage(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }
}
