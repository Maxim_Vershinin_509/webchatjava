package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ShutdownMessage implements Message {
    private final int id;

    public ShutdownMessage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
