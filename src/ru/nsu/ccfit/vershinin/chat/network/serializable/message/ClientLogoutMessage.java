package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ClientLogoutMessage implements Message {
    private final String name;

    public ClientLogoutMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
