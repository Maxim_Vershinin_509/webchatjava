package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class AnswerPingMessage implements Message {
    private final int id;

    public AnswerPingMessage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
