package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ServerSuccessWithIdMessage implements Message {
    private final int id;

    public ServerSuccessWithIdMessage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
