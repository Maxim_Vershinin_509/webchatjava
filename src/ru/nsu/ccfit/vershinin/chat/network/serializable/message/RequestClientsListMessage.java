package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class RequestClientsListMessage implements Message {
    private final int id;

    public RequestClientsListMessage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
