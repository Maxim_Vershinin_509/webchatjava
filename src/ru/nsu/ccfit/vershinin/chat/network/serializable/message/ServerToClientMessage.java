package ru.nsu.ccfit.vershinin.chat.network.serializable.message;

public class ServerToClientMessage implements Message {
    private final String message;
    private final String name;

    public ServerToClientMessage(String message, String name) {
        this.message = message;
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }
}
