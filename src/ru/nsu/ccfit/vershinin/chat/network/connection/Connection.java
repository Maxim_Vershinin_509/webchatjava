package ru.nsu.ccfit.vershinin.chat.network.connection;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.*;

public class Connection implements ConnectionInterface {
    private final Thread inputThread;
    private final Thread outputThread;
    private final ObjectInputStream inputStream;
    private final ObjectOutputStream outputStream;
    private final BlockingDeque<Object> messageDeque = new LinkedBlockingDeque<>();

    public Connection(ConnectionListener listener, String ipAddress, int port) throws IOException {
        this(listener, new Socket(ipAddress, port));
    }

    public Connection(ConnectionListener listener, Socket socket) throws IOException {
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());
        inputThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!inputThread.isInterrupted()) {
                        Object readObject = inputStream.readObject();
                        listener.reactOnMessage(readObject, Connection.this);
                    }
                } catch (ClassNotFoundException | IOException ex){
                    try {
                        listener.disconnect(Connection.this);
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                    }
                }
            }
        });
        inputThread.start();
        outputThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!outputThread.isInterrupted()) {
                        Object message = messageDeque.takeFirst();
                        outputStream.writeObject(message);
                    }
                } catch (IOException ex) {
                    try {
                        listener.disconnect(Connection.this);
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                    }
                } catch (InterruptedException ex) {
                    //
                } finally {
                    try {
                        while (!messageDeque.isEmpty()) {
                            outputStream.writeObject(messageDeque.poll());
                        }
                    } catch (IOException ex) {
                        System.out.println(ex.getLocalizedMessage());
                    } finally {
                        try {
                            outputStream.close();
                            inputStream.close();
                            socket.close();
                        } catch (IOException ex) {
                            System.out.println(ex.getLocalizedMessage());
                        }
                    }
                }
            }
        });
        outputThread.start();
    }

    @Override
    public synchronized void addMessage(Object message) {
        messageDeque.addLast(message);
    }

    @Override
    public synchronized void addPriorityMessage(Object message) {
        messageDeque.addFirst(message);
    }

    @Override
    public synchronized void closeInputThread() {
        inputThread.interrupt();
    }

    @Override
    public synchronized void closeOutputThread() {
        outputThread.interrupt();
    }

}
