package ru.nsu.ccfit.vershinin.chat.network.connection;

import java.io.IOException;

public interface ConnectionListener {
    void reactOnMessage(Object message, Connection connection) throws IOException;

    void disconnect(ConnectionInterface connection) throws Exception;
}
