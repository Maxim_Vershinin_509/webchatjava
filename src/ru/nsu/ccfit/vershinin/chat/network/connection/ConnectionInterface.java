package ru.nsu.ccfit.vershinin.chat.network.connection;

public interface ConnectionInterface {
    void addMessage(Object message);
    void addPriorityMessage(Object message);
    void closeInputThread();
    void closeOutputThread();
}
