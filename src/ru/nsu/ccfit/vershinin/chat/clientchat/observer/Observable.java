package ru.nsu.ccfit.vershinin.chat.clientchat.observer;

public interface Observable {
    enum SelectorGUIUpdate {
        ERROR_UPDATER, CHAT_BOARD_UPDATER, START_CHATTING_UPDATER, EXIT_CHATTING_UPDATER
    }
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers(SelectorGUIUpdate selectorGUIUpdate, String message);
}
