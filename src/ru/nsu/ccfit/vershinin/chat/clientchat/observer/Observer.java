package ru.nsu.ccfit.vershinin.chat.clientchat.observer;

public interface Observer {
    void updateErrorPanel(String errorMessage);

    void updateChatBoard(String message);

    void updateStartChatBoard(String name);

    void updateExitChatting();
}
