package ru.nsu.ccfit.vershinin.chat.clientchat;

import ru.nsu.ccfit.vershinin.chat.clientchat.controller.Controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ChatClient {

    public static void main(String[] args) {
        try {
            Controller controller = new Controller("127.0.0.1", 8886);
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

}
