package ru.nsu.ccfit.vershinin.chat.clientchat.view;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;

class RegistrationDialog extends JDialog {
    private JTextField nicknameField;
    private JCheckBox xmlFlag = new JCheckBox("XML");
    private JCheckBox serializationFlag = new JCheckBox("Serialization");

    boolean isXmlSelected() {
        return xmlFlag.isSelected();
    }

    boolean isSerializationSelected() {
        return serializationFlag.isSelected();
    }

    void setXmlFlag(boolean b) {
        xmlFlag.setSelected(b);
    }

    void setSerializationFlag(boolean b) {
        serializationFlag.setSelected(b);
    }

    String getNickname() {
        return nicknameField.getText();
    }

    RegistrationDialog(Frame frame, Viewer viewer) {
        super(frame, "Registration");

        setLayout(null);

        JLabel textLabel = new JLabel("Enter your nickname:", SwingConstants.CENTER);

        textLabel.setPreferredSize(new Dimension(200, 70));

        textLabel.setBounds(20, 20, 260, 25);

        textLabel.setFont(new Font(null, Font.ITALIC, 18));

        nicknameField = new JTextField();

        nicknameField.setBounds(50, 70, 200, 20);

        nicknameField.setPreferredSize(new Dimension(200, 70));

        nicknameField.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (null == str) return;
                if (getLength() + str.length() <= 10) {
                    for (int i = 0; i < str.length(); i++) {
                        if (Character.isSpaceChar(str.charAt(i))) return;
                    }
                    super.insertString(offs, str, a);
                }
            }
        });

        nicknameField.setActionCommand("enter nickname");

        nicknameField.addActionListener(viewer);

        JButton enterButton = new JButton("Enter");

        enterButton.setActionCommand("enter nickname");

        enterButton.addActionListener(viewer);

        enterButton.setPreferredSize(new Dimension(90, 30));

        enterButton.setBounds(105, 125, 90, 30);

        serializationFlag.setBounds(10, 180, 140, 50);
        serializationFlag.addActionListener(viewer);
        serializationFlag.setActionCommand("serial flag");
        xmlFlag.setBounds(160, 180, 140, 50);
        xmlFlag.addActionListener(viewer);
        xmlFlag.setActionCommand("xml flag");

        add(serializationFlag);
        add(xmlFlag);

        add(enterButton);

        add(textLabel);

        add(nicknameField);

        setPreferredSize(new Dimension(300, 260));

        pack();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
    }

}
