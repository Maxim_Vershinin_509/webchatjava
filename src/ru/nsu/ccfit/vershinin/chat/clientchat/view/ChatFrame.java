package ru.nsu.ccfit.vershinin.chat.clientchat.view;

import javax.swing.*;
import java.awt.*;

class ChatFrame extends JFrame {
    private final RegistrationDialog registrationDialog;
    private final JLabel nickNameLabel = new JLabel();
    private final JTextField msgField;
    private final JTextArea chatArea;
    private final JDialog errorDialog = new JDialog(this, "Error message");
    private final JLabel errorText = new JLabel("", SwingConstants.CENTER);

    private void initErrorDialog(Viewer viewer) {

        errorDialog.setLayout(null);
        errorDialog.setPreferredSize(new Dimension(400, 200));

        errorText.setPreferredSize(new Dimension(250, 100));
        errorText.setBounds(75, 0, 250, 100);
        errorText.setFont(new Font(null, Font.ITALIC, 18));

        errorDialog.add(errorText);

        JButton errorButton = new JButton("Exit");
        errorButton.setPreferredSize((new Dimension(100, 30)));
        errorButton.setBounds(150, 110, 100, 30);
        errorButton.setActionCommand("exit command");
        errorButton.addActionListener(viewer);
        errorDialog.add(errorButton);

        errorDialog.pack();
        setVisibleErrorDialog(false);
        errorDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        errorDialog.setResizable(false);
        errorDialog.setLocationRelativeTo(null);
    }

    ChatFrame(Viewer viewer) {
        super("Chat");
        registrationDialog = new RegistrationDialog(this, viewer);
        setVisibleRegistrationDialog(true);

        initErrorDialog(viewer);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setPreferredSize(new Dimension(1200, 800));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        chatArea = new JTextArea(5, 30);
        chatArea.setEditable(false);
        chatArea.setLineWrap(true);
        chatArea.setWrapStyleWord(true);
        chatArea.setFont(new Font(null, Font.ITALIC, 16));
        JScrollPane scroll = new JScrollPane(chatArea);
        scroll.setPreferredSize(new Dimension(1200, 770));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mainPanel.add(scroll, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        nickNameLabel.setPreferredSize(new Dimension(200, 30));
        nickNameLabel.setHorizontalTextPosition(JLabel.RIGHT);
        mainPanel.add(nickNameLabel, c);

        c.gridx = 1;
        msgField = new JTextField();
        msgField.setPreferredSize(new Dimension(700, 30));
        msgField.setActionCommand("enter message");
        msgField.addActionListener(viewer);
        mainPanel.add(msgField, c);

        c.gridx = 2;
        JButton enterButton = new JButton("Enter");
        enterButton.setPreferredSize(new Dimension(100, 30));
        enterButton.setActionCommand("enter message");
        enterButton.addActionListener(viewer);
        mainPanel.add(enterButton, c);

        c.gridx = 3;
        JButton requestClientsListButton = new JButton("Request Clients List");
        requestClientsListButton.setPreferredSize(new Dimension(200, 30));
        requestClientsListButton.setActionCommand("requestClientsListButton");
        requestClientsListButton.addActionListener(viewer);
        mainPanel.add(requestClientsListButton, c);

        add(mainPanel);

        pack();
        addWindowListener(viewer);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

    }

    String getNickname() {
        return registrationDialog.getNickname();
    }

    String getMessage() {
        return msgField.getText();
    }

    void setVisibleErrorDialog(boolean b) {
        errorDialog.setVisible(b);
    }

    void setNickName(String nickName) {
        nickNameLabel.setText(nickName);
    }

    void printText(String message) {
        SwingUtilities.invokeLater(() -> {
            chatArea.append(message);
            chatArea.setCaretPosition(chatArea.getDocument().getLength());
        });
    }

    boolean isXmlSelected() {
        return registrationDialog.isXmlSelected();
    }

    boolean isSerializationSelected() {
        return registrationDialog.isSerializationSelected();
    }

    void setXmlFlag(boolean b) {
        registrationDialog.setXmlFlag(b);
    }

    void setSerializationFlag(boolean b) {
        registrationDialog.setSerializationFlag(b);
    }

    void setErrorText(String text) {
        errorText.setText(text);
    }

    void setNullMsgField() {
        msgField.setText(null);
    }

    void setVisibleRegistrationDialog(boolean b) {
        registrationDialog.setVisible(b);
    }
}
