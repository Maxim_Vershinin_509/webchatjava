package ru.nsu.ccfit.vershinin.chat.clientchat.view;

import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;
import ru.nsu.ccfit.vershinin.chat.clientchat.controller.ControllerInterface;
import ru.nsu.ccfit.vershinin.chat.clientchat.observer.Observer;

import java.awt.event.*;

public class Viewer implements ActionListener, WindowListener, Observer {

    private final ChatFrame chatFrame;
    private final ControllerInterface controller;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if ("serial flag".equals(actionEvent.getActionCommand())) {
            if (chatFrame.isXmlSelected() && chatFrame.isSerializationSelected()) {
                chatFrame.setXmlFlag(false);
            } else {
                chatFrame.setSerializationFlag(true);
            }
        } else if ("xml flag".equals(actionEvent.getActionCommand())) {
            if (chatFrame.isSerializationSelected() && chatFrame.isXmlSelected()) {
                chatFrame.setSerializationFlag(false);
            } else {
                chatFrame.setXmlFlag(true);
            }
        } else if ("enter nickname".equals(actionEvent.getActionCommand())) {
            if (chatFrame.getNickname().isEmpty()) return;
            if (chatFrame.isXmlSelected()) {
                chatFrame.setVisibleRegistrationDialog(false);
                controller.registration(chatFrame.getNickname(), Client.ClientType.XML_TYPE);
            } else if (chatFrame.isSerializationSelected()) {
                chatFrame.setVisibleRegistrationDialog(false);
                controller.registration(chatFrame.getNickname(), Client.ClientType.SERIALIZATION_TYPE);
            }
        } else if ("enter message".equals(actionEvent.getActionCommand())) {
            if (chatFrame.getMessage().isEmpty()) return;
            controller.sendMessage(chatFrame.getMessage());
        } else if ("requestClientsListButton".equals(actionEvent.getActionCommand())) {
            controller.requestClientsList();
        } else if ("exit command".equals(actionEvent.getActionCommand())) {
            chatFrame.dispose();
            controller.exit();
        }
    }

    public Viewer(ControllerInterface controller) {
        this.controller = controller;
        chatFrame = new ChatFrame(this);
    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        controller.shutdown();
    }

    @Override
    public void updateErrorPanel(String errorMessage) {
        chatFrame.setVisibleRegistrationDialog(false);
        chatFrame.setVisible(false);
        chatFrame.setErrorText(errorMessage);
        chatFrame.setVisibleErrorDialog(true);
    }

    @Override
    public void updateChatBoard(String message) {
        chatFrame.setNullMsgField();
        chatFrame.printText(message);
    }

    @Override
    public void updateStartChatBoard(String name) {
        chatFrame.setNickName(name);
        chatFrame.setVisible(true);

    }

    @Override
    public void updateExitChatting() {
        chatFrame.dispose();
        controller.exit();
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
