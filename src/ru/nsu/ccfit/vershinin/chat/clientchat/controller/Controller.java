package ru.nsu.ccfit.vershinin.chat.clientchat.controller;

import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;
import ru.nsu.ccfit.vershinin.chat.clientchat.client.SerializableClient;
import ru.nsu.ccfit.vershinin.chat.clientchat.client.XMLClient;
import ru.nsu.ccfit.vershinin.chat.clientchat.view.Viewer;

import java.io.IOException;
import java.net.Socket;

public class Controller implements ControllerInterface {
    private final Viewer viewer;
    private Client client;
    private final Socket socket;

    public Controller(String ipAddress, int port) throws IOException {
        socket = new Socket(ipAddress, port);
        this.viewer = new Viewer( this);
    }

    @Override
    public void sendMessage(String str) {
        client.sendMessage(str);
    }

    @Override
    public void registration(String str, Client.ClientType clientType) {
        try {
            if (clientType.equals(Client.ClientType.SERIALIZATION_TYPE)) {
                client = new SerializableClient(socket);
            } else {
                client = new XMLClient(socket);
            }
            client.registerObserver(viewer);
            client.registration(str, clientType);
        } catch (IOException ex) {
            viewer.updateErrorPanel("Error opening I/O streams.\n");
        }
    }

    @Override
    public void requestClientsList() {
        client.requestClientsList();
    }

    @Override
    public void shutdown() {
        client.shutdown();
    }

    @Override
    public void exit() {
        if (client != null) {
            client.disconnect();
        }
    }
}
