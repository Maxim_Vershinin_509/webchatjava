package ru.nsu.ccfit.vershinin.chat.clientchat.controller;

import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;

public interface ControllerInterface {
    void sendMessage(String str);
    void registration(String str, Client.ClientType clientType);
    void requestClientsList();
    void shutdown();
    void exit();
}
