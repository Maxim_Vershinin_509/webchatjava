package ru.nsu.ccfit.vershinin.chat.clientchat.client;

import ru.nsu.ccfit.vershinin.chat.clientchat.observer.Observable;
import ru.nsu.ccfit.vershinin.chat.clientchat.observer.Observer;
import ru.nsu.ccfit.vershinin.chat.network.connection.Connection;

import java.util.ArrayList;

public abstract class Client implements Observable {
    public enum ClientType {
        XML_TYPE, SERIALIZATION_TYPE
    }

    private final ArrayList<Observer> observers = new ArrayList<>();
    int id;
    String name;
    Connection connection;

    public abstract void sendMessage(String str);
    public abstract void registration(String str, ClientType clientType);
    public abstract void requestClientsList();
    public abstract void shutdown();

    public void disconnect() {
        connection.closeInputThread();
        connection.closeOutputThread();
    }

    @Override
    public void notifyObservers(SelectorGUIUpdate selectorGUIUpdate, String message) {
        for (Observer o : observers) {
            if (selectorGUIUpdate.equals(SelectorGUIUpdate.CHAT_BOARD_UPDATER)) {
                o.updateChatBoard(message);
            } else if (selectorGUIUpdate.equals(SelectorGUIUpdate.ERROR_UPDATER)) {
                o.updateErrorPanel(message);
            } else if (selectorGUIUpdate.equals(SelectorGUIUpdate.START_CHATTING_UPDATER)) {
                o.updateStartChatBoard(message);
            } else if (selectorGUIUpdate.equals(SelectorGUIUpdate.EXIT_CHATTING_UPDATER)) {
                o.updateExitChatting();
            }
        }
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

}
