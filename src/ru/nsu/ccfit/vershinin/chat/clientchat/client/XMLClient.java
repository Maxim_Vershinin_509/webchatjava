package ru.nsu.ccfit.vershinin.chat.clientchat.client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.nsu.ccfit.vershinin.chat.network.connection.Connection;
import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionInterface;
import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionListener;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;

public class XMLClient extends Client implements ConnectionListener {

    public XMLClient(String ipAddress, int port) throws IOException {
        this.connection = new Connection(this, ipAddress, port);
    }

    public XMLClient(Socket socket) throws IOException {
        this.connection = new Connection(this, socket);
    }

    private String transformDocToStr(Document document) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter stringWriter = new StringWriter();

        transformer.transform(new DOMSource(document), new StreamResult(stringWriter));

        return stringWriter.getBuffer().toString();

    }

    private Document createDocOnlyWithId(String text) throws ParserConfigurationException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element command = doc.createElement("command");
        command.setAttribute("name", text);
        doc.appendChild(command);

        Element session = doc.createElement("session");
        session.setTextContent(Integer.toString(id));
        command.appendChild(session);
        return doc;
    }

    private void parseEventServerMessage(Element el) {
        String nickname = el.getElementsByTagName("name").item(0).getTextContent();
        if (el.getAttributeNode("name").getValue().equals("userlogin")) {
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, "New user arrived: \"" + nickname + "\"\n");
        } else if (el.getAttributeNode("name").getValue().equals("userlogout")) {
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER,  "User with nickname \"" + nickname + "\" disconnected\n");
        } else {
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, nickname + ": " +
                    el.getElementsByTagName("message").item(0).getTextContent() + "\n");
        }
    }

    private void parseSuccessServerMessage(Element el) {
        if (el.getElementsByTagName("user").getLength() != 0) {
            final NodeList usersInfo = el.getElementsByTagName("user");
            StringBuilder msg = new StringBuilder();

            for (int i = 0; i < usersInfo.getLength(); i++) {
                msg.append("name - ").append(el.getElementsByTagName("name").item(i).getTextContent()).append(", type - ")
                                    .append(el.getElementsByTagName("type").item(i).getTextContent()).append("; ");
            }
            msg.deleteCharAt(msg.length()-1);
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, "Online clients: " + msg.toString() + "\n");
        } else if (el.getElementsByTagName("session").getLength() != 0) {
            this.id = Integer.parseInt(el.getElementsByTagName("session").item(0).getTextContent());
            notifyObservers(SelectorGUIUpdate.START_CHATTING_UPDATER, name + ":");
        } else {
            notifyObservers(SelectorGUIUpdate.EXIT_CHATTING_UPDATER, null);
        }
    }

    private void parseErrorServerMessage(Element el) {
        notifyObservers(SelectorGUIUpdate.ERROR_UPDATER, el.getElementsByTagName("message").item(0).getTextContent() + ".\n");
    }

    private void sendAnswerPingMessage(Element el) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element command = doc.createElement("command");
        command.setAttribute("name", el.getTagName());
        doc.appendChild(command);

        Element session = doc.createElement("session");
        session.setTextContent(Integer.toString(id));
        command.appendChild(session);

        connection.addMessage(transformDocToStr(doc));
    }

    @Override
    public void sendMessage(String str) {
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setNamespaceAware(true);
            Document doc = builderFactory.newDocumentBuilder().newDocument();
            Element command = doc.createElement("command");
            command.setAttribute("name", "message");
            doc.appendChild(command);

            Element message = doc.createElement("message");
            message.setTextContent(str);
            command.appendChild(message);

            Element session = doc.createElement("session");
            session.setTextContent(Integer.toString(id));
            command.appendChild(session);

            connection.addMessage(transformDocToStr(doc));

        } catch (ParserConfigurationException | TransformerException ex) {
            System.out.println(ex.getLocalizedMessage());
        }

    }

    @Override
    public void registration(String str, ClientType clientType) {
        this.name = str;
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setNamespaceAware(true);
            Document doc = builderFactory.newDocumentBuilder().newDocument();
            Element command = doc.createElement("command");
            command.setAttribute("name", "login");
            doc.appendChild(command);

            Element name = doc.createElement("name");
            name.setTextContent(str);
            command.appendChild(name);

            Element type = doc.createElement("type");
            if (clientType.equals(ClientType.SERIALIZATION_TYPE)) {
                type.setTextContent("Serialization");
            } else {
                type.setTextContent("XML");
            }
            command.appendChild(type);

            connection.addMessage(transformDocToStr(doc));

        } catch (ParserConfigurationException | TransformerException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    @Override
    public void requestClientsList() {
        try {
            connection.addMessage(transformDocToStr(createDocOnlyWithId("list")));
        } catch (ParserConfigurationException | TransformerException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    @Override
    public void shutdown() {
        try {
            connection.addMessage(transformDocToStr(createDocOnlyWithId("logout")));
        } catch (ParserConfigurationException | TransformerException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }


    @Override
    public void reactOnMessage(Object message, Connection connection) throws IOException {
        try {
            if (message instanceof String) {
                String msg = (String) message;
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                builderFactory.setNamespaceAware(true);
                Document doc = builderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(msg)));
                Element el = doc.getDocumentElement();
                switch (el.getTagName()) {
                    case "event":
                        parseEventServerMessage(el);
                        break;
                    case "success":
                        parseSuccessServerMessage(el);
                        break;
                    case "error":
                        parseErrorServerMessage(el);
                        break;
                    case "ping":
                        sendAnswerPingMessage(el);
                }
            }
        } catch (ParserConfigurationException | SAXException | TransformerException ex) {
            notifyObservers(SelectorGUIUpdate.ERROR_UPDATER, ex.getLocalizedMessage() + "\n");
        }
    }

    @Override
    public void disconnect(ConnectionInterface connection) {
        notifyObservers(SelectorGUIUpdate.ERROR_UPDATER, "Server disconnected.\n");
    }
}
