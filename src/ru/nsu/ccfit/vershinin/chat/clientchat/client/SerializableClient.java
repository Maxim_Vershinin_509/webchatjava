package ru.nsu.ccfit.vershinin.chat.clientchat.client;

import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionInterface;
import ru.nsu.ccfit.vershinin.chat.network.connection.Connection;
import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionListener;
import ru.nsu.ccfit.vershinin.chat.network.serializable.message.*;


import java.io.IOException;
import java.net.Socket;

public class SerializableClient extends Client implements ConnectionListener {

    public SerializableClient(String ipAddress, int port) throws IOException {
        connection = new Connection(this, ipAddress, port);
    }

    public SerializableClient(Socket socket) throws IOException {
        connection = new Connection(this, socket);
    }

    @Override
    public void sendMessage(String str) {
        connection.addMessage(new ClientToServerMessage(str, id));
    }

    @Override
    public void registration(String name, ClientType clientType) {
        this.name = name;
        connection.addMessage(new RegistrationMessage(name, clientType));
    }

    @Override
    public void requestClientsList() {
        connection.addMessage(new RequestClientsListMessage(id));
    }

    @Override
    public void shutdown() {
        connection.addMessage(new ShutdownMessage(id));
    }


    @Override
    public void reactOnMessage(Object message, Connection connection) {
        if (message instanceof ServerErrorMessage) {
            ServerErrorMessage serverErrorMessage = (ServerErrorMessage) message;
            notifyObservers(SelectorGUIUpdate.ERROR_UPDATER, serverErrorMessage.getReason() + ".\n");
        } else if (message instanceof ServerSuccessWithIdMessage) {
            ServerSuccessWithIdMessage serverSuccessWithIdMessage = (ServerSuccessWithIdMessage) message;
            this.id = serverSuccessWithIdMessage.getId();
            notifyObservers(SelectorGUIUpdate.START_CHATTING_UPDATER, name + ":");
        } else if (message instanceof ServerSuccessListMessage) {
            ServerSuccessListMessage serverSuccessListMessage = (ServerSuccessListMessage) message;
            StringBuilder clientsList = new StringBuilder();
            for (ServerSuccessListMessage.NameAndType nameAndType : serverSuccessListMessage.getListNames()) {
                clientsList.append(nameAndType);
            }
            clientsList.deleteCharAt(clientsList.length()-1);
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, "Online clients: " + clientsList.toString() + "\n");
        } else if (message instanceof ServerToClientMessage) {
            ServerToClientMessage serverToClientMessage = (ServerToClientMessage) message;
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, serverToClientMessage.getName() + ": " + serverToClientMessage.getMessage() + "\n");
        } else if (message instanceof ClientLogoutMessage) {
            ClientLogoutMessage clientLogoutMessage = (ClientLogoutMessage) message;
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER,  "User with nickname \"" + clientLogoutMessage.getName() + "\" disconnected\n");
        } else if (message instanceof ServerSuccessMessage) {
            notifyObservers(SelectorGUIUpdate.EXIT_CHATTING_UPDATER, null);
        } else if (message instanceof NewClientMessage) {
            NewClientMessage newClientMessage = (NewClientMessage) message;
            notifyObservers(SelectorGUIUpdate.CHAT_BOARD_UPDATER, "New user arrived: \"" + newClientMessage.getNickname() + "\"\n");
        } else if (message instanceof PingMessage) {
            connection.addPriorityMessage(new AnswerPingMessage(id));
        }
    }

    @Override
    public void disconnect(ConnectionInterface connection) {
        notifyObservers(SelectorGUIUpdate.ERROR_UPDATER, "Server disconnected.\n");
    }
}
