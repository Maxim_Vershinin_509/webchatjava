package ru.nsu.ccfit.vershinin.chat.serverchat.server;

import ru.nsu.ccfit.vershinin.chat.network.serializable.message.Message;

import java.util.concurrent.ConcurrentLinkedDeque;

class MessagesHistory {
    private final ConcurrentLinkedDeque<Message> serialMsgHistory = new ConcurrentLinkedDeque<>();
    private final ConcurrentLinkedDeque<String> xmlMsgHistory = new ConcurrentLinkedDeque<>();

    void addSerialMsg(Message msg) {
        if (serialMsgHistory.size() == 10) {
            serialMsgHistory.removeFirst();
        }
        serialMsgHistory.addLast(msg);
    }

    void addXmlMsg(String str) {
        if (xmlMsgHistory.size() == 10) {
            xmlMsgHistory.removeFirst();
        }
        xmlMsgHistory.addLast(str);
    }

    ConcurrentLinkedDeque<Message> getSerialMsgHistory() {
        return serialMsgHistory;
    }

    ConcurrentLinkedDeque<String> getXmlMsgHistory() {
        return xmlMsgHistory;
    }
}
