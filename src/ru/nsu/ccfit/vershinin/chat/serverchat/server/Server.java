package ru.nsu.ccfit.vershinin.chat.serverchat.server;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import ru.nsu.ccfit.vershinin.chat.clientchat.client.Client;
import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionInterface;
import ru.nsu.ccfit.vershinin.chat.network.connection.Connection;
import ru.nsu.ccfit.vershinin.chat.network.connection.ConnectionListener;
import ru.nsu.ccfit.vershinin.chat.network.serializable.message.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements ConnectionListener {


    private static class InfoAboutClient {

        private final String name;
        private final ConnectionInterface connection;
        InfoAboutClient(String name, ConnectionInterface connection) {
            this.name = name;
            this.connection = connection;
        }

        String getName() {
            return name;
        }

        ConnectionInterface getConnection() {
            return connection;
        }

    }

    private static class IdAndTypeClient {
        private final int id;
        private final Client.ClientType clientType;

        IdAndTypeClient(int id, Client.ClientType clientType) {
            this.id = id;
            this.clientType = clientType;
        }

        int getId() {
            return id;
        }

        Client.ClientType getClientType() {
            return clientType;
        }
    }

    private final int timeoutForCheck = 1800000;
    private final int timeoutForAnswer = 120000;
    private final AtomicInteger globalId = new AtomicInteger(0);
    private final int MAX_COUNT_CLIENTS = 15;
    private final ConcurrentHashMap<Integer, InfoAboutClient> serializableClientsTable = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, InfoAboutClient> xmlClientsTable = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<ConnectionInterface, IdAndTypeClient> clientsIdTable = new ConcurrentHashMap<>();
    private final Set<String> nameTable = ConcurrentHashMap.newKeySet();
    private final LinkedBlockingQueue<Message> serialMessageForAll = new LinkedBlockingQueue<>();
    private final LinkedBlockingQueue<String> xmlMessageForAll = new LinkedBlockingQueue<>();
    private final ConcurrentHashMap<Integer, Boolean> tableLiveClients = new ConcurrentHashMap<>();
    private final MessagesHistory messagesHistory = new MessagesHistory();

    private ArrayList<ServerSuccessListMessage.NameAndType> createUsersNameList() {
        ArrayList<ServerSuccessListMessage.NameAndType> listName = new ArrayList<>();
        for (Map.Entry<ConnectionInterface, IdAndTypeClient> aboutClient: clientsIdTable.entrySet()) {
            if (serializableClientsTable.get(aboutClient.getValue().getId()) == null) {
                listName.add(new ServerSuccessListMessage.NameAndType(xmlClientsTable.get(aboutClient.getValue().getId()).getName(),
                        aboutClient.getValue().getClientType()));
            } else {
                listName.add(new ServerSuccessListMessage.NameAndType(serializableClientsTable.get(aboutClient.getValue().getId()).getName(),
                        aboutClient.getValue().getClientType()));
            }
        }
        return listName;
    }

    private String transformDocToStr(Document document) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter stringWriter = new StringWriter();

        transformer.transform(new DOMSource(document), new StreamResult(stringWriter));

        return stringWriter.getBuffer().toString();

    }

    private void sendLastMessage(ConnectionInterface connection) throws Exception {
        connection.closeInputThread();
        String name;
        if (clientsIdTable.get(connection).getClientType().equals(Client.ClientType.SERIALIZATION_TYPE)) {
            connection.addMessage(new ServerSuccessMessage());
            name = serializableClientsTable.get(clientsIdTable.get(connection).getId()).getName();
            serializableClientsTable.remove(clientsIdTable.get(connection).getId());
        } else {
            connection.addMessage(createServerSuccessXmlMessage());
            name = xmlClientsTable.get(clientsIdTable.get(connection).getId()).getName();
            xmlClientsTable.remove(clientsIdTable.get(connection).getId());
        }
        nameTable.remove(name);
        tableLiveClients.remove(clientsIdTable.get(connection).getId());
        clientsIdTable.remove(connection);
        connection.closeOutputThread();
        ClientLogoutMessage serMsg = new ClientLogoutMessage(name);
        serialMessageForAll.add(serMsg);
        messagesHistory.addSerialMsg(serMsg);
        String xmlMsg = createClientLogoutXmlMessage(name);
        xmlMessageForAll.add(xmlMsg);
        messagesHistory.addXmlMsg(xmlMsg);
    }

    private String createSimpleXmlMessage(String element) throws Exception {
        String str;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element ping = doc.createElement(element);
        doc.appendChild(ping);

        str = transformDocToStr(doc);

        return str;
    }

    private String createPingXmlMessage() throws Exception {
        return createSimpleXmlMessage("ping");
    }

    private String createServerXmlMessageAboutCountClients(String attributeName,String nickname) throws Exception {
        String str;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element event = doc.createElement("event");
        event.setAttribute("name", attributeName);
        doc.appendChild(event);

        Element name = doc.createElement("name");
        name.setTextContent(nickname);
        event.appendChild(name);

        str = transformDocToStr(doc);

        return str;
    }

    private String createNewClientXmlMessage(String nickname) throws Exception {
        return createServerXmlMessageAboutCountClients("userlogin", nickname);
    }

    private String createClientLogoutXmlMessage(String nickname) throws Exception {
        return createServerXmlMessageAboutCountClients("userlogout", nickname);
    }

    private String createServerToClientXmlMessage(String msg, String nickname) throws Exception {
        String str;
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Element event = doc.createElement("event");
        event.setAttribute("name", "message");
        doc.appendChild(event);

        Element message = doc.createElement("message");
        message.setTextContent(msg);
        event.appendChild(message);

        Element name = doc.createElement("name");
        name.setTextContent(nickname);
        event.appendChild(name);

        str = transformDocToStr(doc);

        return str;
    }

    private String createServerSuccessXmlMessage() throws Exception{
        return createSimpleXmlMessage("success");
    }

    private String createServerSuccessListXmlMessage(ArrayList<ServerSuccessListMessage.NameAndType> listname) throws Exception {
        String str;

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element success = doc.createElement("success");
        doc.appendChild(success);

        Element listusers = doc.createElement("listusers");
        success.appendChild(listusers);

        for (ServerSuccessListMessage.NameAndType nicknameAndType : listname) {
            Element user = doc.createElement("user");
            listusers.appendChild(user);

            Element name = doc.createElement("name");
            name.setTextContent(nicknameAndType.getName());
            user.appendChild(name);

            Element type = doc.createElement("type");
            if (nicknameAndType.getClientType().equals(Client.ClientType.SERIALIZATION_TYPE)) {
                type.setTextContent("Serialization");
            } else {
                type.setTextContent("XML");
            }
            user.appendChild(type);
        }

        str = transformDocToStr(doc);


        return str;
    }

    private String createServerSuccessWithIdXmlMessage(int id) throws Exception {
        String str;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element success = doc.createElement("success");
        doc.appendChild(success);

        Element session = doc.createElement("session");
        session.setTextContent(Integer.toString(id));
        success.appendChild(session);

        str = transformDocToStr(doc);


        return str;
    }

    private String createServerErrorXmlMessage(String reason) throws Exception {
        String str;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().newDocument();
        Element error = doc.createElement("error");
        doc.appendChild(error);

        Element message = doc.createElement("message");
        message.setTextContent(reason);
        error.appendChild(message);

        str = transformDocToStr(doc);

        return str;
    }

    private void parseRegistrationMessage(Element el, Connection senderOfMessage) throws Exception {
        String nickname = el.getElementsByTagName("name").item(0).getTextContent();
        String type = el.getElementsByTagName("type").item(0).getTextContent();
        if (nameTable.size() >= MAX_COUNT_CLIENTS) {
            if (type.equals("Serialization")) {
                senderOfMessage.addMessage(new ServerErrorMessage("User limit exceeded"));
            } else {
                senderOfMessage.addMessage(createServerErrorXmlMessage("User limit exceeded"));
            }
            senderOfMessage.closeInputThread();
            senderOfMessage.closeOutputThread();
        } else if (nameTable.contains(nickname)) {
            if (type.equals("Serialization")) {
                senderOfMessage.addMessage(new ServerErrorMessage("Bad nickname for client"));
            } else {
                senderOfMessage.addMessage(createServerErrorXmlMessage("Bad nickname for client"));
            }
            senderOfMessage.closeInputThread();
            senderOfMessage.closeOutputThread();
        } else {
            int id = globalId.getAndIncrement();
            Object success;
            Client.ClientType clientType;
            if (type.equals("Serialization")) {
                clientType = Client.ClientType.SERIALIZATION_TYPE;
                success = new ServerSuccessWithIdMessage(id);
                serializableClientsTable.put(id, new InfoAboutClient(nickname, senderOfMessage));
                senderOfMessage.addMessage(success);
                for (Message msg : messagesHistory.getSerialMsgHistory()) {
                    senderOfMessage.addMessage(msg);
                }
            } else {
                clientType = Client.ClientType.XML_TYPE;
                success = createServerSuccessWithIdXmlMessage(id);
                xmlClientsTable.put(id, new InfoAboutClient(nickname, senderOfMessage));
                senderOfMessage.addMessage(success);
                for (String msg : messagesHistory.getXmlMsgHistory()) {
                    senderOfMessage.addMessage(msg);
                }
            }
            clientsIdTable.put(senderOfMessage, new IdAndTypeClient(id, clientType));
            nameTable.add(nickname);
            tableLiveClients.put(id, true);
            NewClientMessage newClientMessage = new NewClientMessage(nickname);
            serialMessageForAll.add(newClientMessage);
            messagesHistory.addSerialMsg(newClientMessage);
            String msg = createNewClientXmlMessage(nickname);
            xmlMessageForAll.add(msg);
            messagesHistory.addXmlMsg(msg);
        }

    }

    private void parseLogoutMessage(Element el) throws Exception {
        int id = Integer.parseInt(el.getElementsByTagName("session").item(0).getTextContent());
        ConnectionInterface connection;
        if (serializableClientsTable.get(id) == null) {
            connection = xmlClientsTable.get(id).getConnection();
        } else {
            connection = serializableClientsTable.get(id).getConnection();
        }
        sendLastMessage(connection);
    }

    private void parseRequestClientsListMessage(Element el) throws Exception {
        int id = Integer.parseInt(el.getElementsByTagName("session").item(0).getTextContent());
        if (xmlClientsTable.get(id) == null) {
            serializableClientsTable.get(id).getConnection().addMessage(new ServerSuccessListMessage(createUsersNameList()));
        } else {
            xmlClientsTable.get(id).getConnection().addMessage(createServerSuccessListXmlMessage(createUsersNameList()));
        }
    }

    private void parseClientToServerMessage(Element el) throws Exception {
        String message = el.getElementsByTagName("message").item(0).getTextContent();
        int id = Integer.parseInt(el.getElementsByTagName("session").item(0).getTextContent());
        ServerToClientMessage serMsg = new ServerToClientMessage(message, xmlClientsTable.get(id).getName());
        serialMessageForAll.add(serMsg);
        messagesHistory.addSerialMsg(serMsg);
        String xmlMsg = createServerToClientXmlMessage(message, xmlClientsTable.get(id).getName());
        xmlMessageForAll.add(xmlMsg);
        messagesHistory.addXmlMsg(xmlMsg);
    }

    private void parseAnswerPingMessage(Element el) {
        int id = Integer.parseInt(el.getElementsByTagName("session").item(0).getTextContent());
        tableLiveClients.put(id, true);
    }

    private void reactOnXmlMessage(String message, Connection senderOfMessage) throws Exception {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        Document doc = builderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(message)));
        Element el = doc.getDocumentElement();
        switch (el.getAttributeNode("name").getValue()) {
            case "message":
                parseClientToServerMessage(el);
                break;
            case "logout":
                parseLogoutMessage(el);
                break;
            case "list":
                parseRequestClientsListMessage(el);
                break;
            case "login":
                parseRegistrationMessage(el, senderOfMessage);
                break;
            case "ping":
                parseAnswerPingMessage(el);
                break;
        }
    }

    private void reactOnAnswerPingMessage(AnswerPingMessage message) {
        int id = message.getId();
        tableLiveClients.put(id, true);
    }

    private void reactOnRegistrationMessage(RegistrationMessage registrationMessage, Connection senderOfMessage) throws Exception {
        if (nameTable.size() > MAX_COUNT_CLIENTS) {
            if (registrationMessage.getClientType().equals(Client.ClientType.SERIALIZATION_TYPE)) {
                senderOfMessage.addMessage(new ServerErrorMessage("User limit exceeded"));
            } else {
                senderOfMessage.addMessage(createServerErrorXmlMessage("User limit exceeded"));
            }
            senderOfMessage.closeInputThread();
            senderOfMessage.closeOutputThread();
        } else if (nameTable.contains(registrationMessage.getName())) {
            if (registrationMessage.getClientType().equals(Client.ClientType.SERIALIZATION_TYPE)) {
                senderOfMessage.addMessage(new ServerErrorMessage("Bad nickname for client"));
            } else {
                senderOfMessage.addMessage(createServerErrorXmlMessage("Bad nickname for client"));
            }
            senderOfMessage.closeInputThread();
            senderOfMessage.closeOutputThread();
        } else {
            int id = globalId.getAndIncrement();
            Object success;
            if (registrationMessage.getClientType().equals(Client.ClientType.SERIALIZATION_TYPE)) {
                success = new ServerSuccessWithIdMessage(id);
                serializableClientsTable.put(id, new InfoAboutClient(registrationMessage.getName(), senderOfMessage));
                senderOfMessage.addMessage(success);
                for (Message msg : messagesHistory.getSerialMsgHistory()) {
                    senderOfMessage.addMessage(msg);
                }
            } else {
                success = createServerSuccessWithIdXmlMessage(id);
                xmlClientsTable.put(id, new InfoAboutClient(registrationMessage.getName(), senderOfMessage));
                senderOfMessage.addMessage(success);
                for (String msg : messagesHistory.getXmlMsgHistory()) {
                    senderOfMessage.addMessage(msg);
                }
            }
            clientsIdTable.put(senderOfMessage, new IdAndTypeClient(id, registrationMessage.getClientType()));
            nameTable.add(registrationMessage.getName());
            tableLiveClients.put(id, true);
            NewClientMessage newClientMessage = new NewClientMessage(registrationMessage.getName());
            serialMessageForAll.add(newClientMessage);
            messagesHistory.addSerialMsg(newClientMessage);
            String msg = createNewClientXmlMessage(registrationMessage.getName());
            xmlMessageForAll.add(msg);
            messagesHistory.addXmlMsg(msg);
        }
    }

    private void reactOnShutdownMessage(ShutdownMessage shutdownMessage) throws Exception {
        ConnectionInterface thisConnection = serializableClientsTable.get(shutdownMessage.getId()).getConnection();
        sendLastMessage(thisConnection);
    }

    private void reactOnRequestClientsListMessage(RequestClientsListMessage requestClientsListMessage) throws Exception {
        if (xmlClientsTable.get(requestClientsListMessage.getId()) == null) {
            serializableClientsTable.get(requestClientsListMessage.getId()).getConnection().addMessage(new ServerSuccessListMessage(createUsersNameList()));
        } else {
            xmlClientsTable.get(requestClientsListMessage.getId()).getConnection().addMessage(createServerSuccessListXmlMessage(createUsersNameList()));
        }
    }

    private void reactOnClientToServerMessage(ClientToServerMessage clientToServerMessage) throws Exception {
        ServerToClientMessage serMsg = new ServerToClientMessage(clientToServerMessage.getMessage(),
                serializableClientsTable.get(clientToServerMessage.getId()).getName());
        serialMessageForAll.add(serMsg);
        messagesHistory.addSerialMsg(serMsg);
        String xmlMsg = createServerToClientXmlMessage(clientToServerMessage.getMessage(),
                serializableClientsTable.get(clientToServerMessage.getId()).getName());
        xmlMessageForAll.add(xmlMsg);
        messagesHistory.addXmlMsg(xmlMsg);
    }

    private void reactOnSerializationMessage(Message message, Connection senderOfMessage) throws Exception {
        if (message instanceof RegistrationMessage) {
            reactOnRegistrationMessage((RegistrationMessage) message, senderOfMessage);
        } else if (message instanceof ShutdownMessage) {
            reactOnShutdownMessage((ShutdownMessage) message);
        } else if (message instanceof RequestClientsListMessage) {
            reactOnRequestClientsListMessage((RequestClientsListMessage) message);
        } else if (message instanceof ClientToServerMessage) {
            reactOnClientToServerMessage((ClientToServerMessage) message);
        } else if (message instanceof AnswerPingMessage) {
            reactOnAnswerPingMessage((AnswerPingMessage) message);
        }
    }

    public Server(int port1) {
        new Thread(() -> {
            try (ServerSocket serverSocket = new ServerSocket(port1)) {
                while (true) {
                    try {
                        new Connection(Server.this, serverSocket.accept());
                    } catch (IOException ex) {
                        System.out.println(ex.getLocalizedMessage());
                    }
                }
            } catch (IOException ex) {
                System.out.println(ex.getLocalizedMessage());
            }
        }).start();

        new Thread(() -> {
            try {
                while (true) {
                    Object message = serialMessageForAll.take();
                    for (Map.Entry<Integer, InfoAboutClient> entry : serializableClientsTable.entrySet()) {
                        entry.getValue().getConnection().addMessage(message);
                    }
                }
            } catch (InterruptedException ex) {
                System.out.println(ex.getLocalizedMessage());
            }
        }).start();

        new Thread(() -> {
            try {
                while (true) {
                    Object message = xmlMessageForAll.take();
                    for (Map.Entry<Integer, InfoAboutClient> entry : xmlClientsTable.entrySet()) {
                        entry.getValue().getConnection().addMessage(message);
                    }
                }
            } catch (InterruptedException ex) {
                System.out.println(ex.getLocalizedMessage());
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(timeoutForCheck);
                    for (Map.Entry<Integer, InfoAboutClient> entry : serializableClientsTable.entrySet()) {
                        entry.getValue().getConnection().addPriorityMessage(new PingMessage());
                    }
                    for (Map.Entry<Integer, InfoAboutClient> entry : xmlClientsTable.entrySet()) {
                        entry.getValue().getConnection().addPriorityMessage(createPingXmlMessage());
                    }
                    Thread.sleep(timeoutForAnswer);
                    for (Map.Entry<Integer, Boolean> entry : tableLiveClients.entrySet()) {
                        if (entry.getValue()) {
                            tableLiveClients.put(entry.getKey(), false);
                        } else {
                             if (serializableClientsTable.get(entry.getKey()) != null) {
                                 disconnect(serializableClientsTable.get(entry.getKey()).getConnection());
                             } else {
                                 disconnect(xmlClientsTable.get(entry.getKey()).getConnection());
                             }
                        }
                    }

                } catch (Exception ex) {
                    System.out.println(ex.getLocalizedMessage());
                }
            }
        }).start();

    }

    @Override
    public void reactOnMessage(Object message, Connection senderOfMessage) {
        try {
            if (message instanceof Message) {
                reactOnSerializationMessage((Message) message, senderOfMessage);
            } else if (message instanceof String) {
                reactOnXmlMessage((String) message, senderOfMessage);
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            senderOfMessage.closeInputThread();
            senderOfMessage.closeOutputThread();
        }
    }

    @Override
    public void disconnect(ConnectionInterface connection) throws Exception {
        if (clientsIdTable.get(connection) == null) {
            connection.closeInputThread();
            connection.closeOutputThread();
        } else {
            sendLastMessage(connection);
        }
    }
}
